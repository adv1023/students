I am Phillip McKnight, and I am a senior majoring in Computer Science. I worked in automotive manufacturing for 7 years before 
I was able to go back to school and for a year and a half while I was in school.  Now I am a full time student and work part 
time as a CS tutor at Pellissippi State Community College. I enjoy spending time with my wife and son (which is a rarity), playing 
games (board and video), and anything that can be done outside.