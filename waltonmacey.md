My name is Walton Macey and I am a first year graduate student pursuing a Master's in Computer Science. 
I graduated in Wireless Software Engineering from Auburn University and have spent the past year working
for an advertising tech company based in California. I go by Wally and am excited to spend the next year 
or so furthering my understanding of software development and data analytics.
